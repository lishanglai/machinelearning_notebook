# References
可以自行在下属列表找找到适合自己的学习资料，虽然罗列的比较多，但是个人最好选择一个深入阅读、练习。当练习到一定程度，可以再看看其他的资料，这样弥补单一学习资料可能存在的欠缺。

列表等在 https://gitee.com/pi-lab/pilab_research_fields/blob/master/references/ML_References.md



## References

* 22 个神经网络结构设计/可视化工具
	- https://www.toutiao.com/i6836884346155041292/
	- https://github.com/ashishpatel26/Tools-to-Design-or-Visualize-Architecture-of-Neural-Network

* CNN 可视化工具 https://m.toutiaocdn.com/group/6822123587156050435
	- https://poloclub.github.io/cnn-explainer/
	- https://github.com/poloclub/cnn-explainer
	
* 一款图像转卡通的Python项目，超级值得你练手
	- https://www.toutiao.com/a6821299115175969287/
	- https://github.com/minivision-ai/photo2cartoon
